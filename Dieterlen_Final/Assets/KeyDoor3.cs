﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class KeyDoor3 : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") && GameControlScript.keyCount >= 3)
        {
            GameControlScript.keyCount--;
            gameObject.SetActive(false);
            SceneManager.LoadScene("StartMenu");
        }
    }
}
