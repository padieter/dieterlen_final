﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    private CircleCollider2D Attack;
    Animator m_Anim;

    void Start()
    {
        Attack = GetComponent<CircleCollider2D>();
        m_Anim = GetComponent<Animator>();
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Attack.enabled = !Attack.enabled;
        }

        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            Attack.enabled = !Attack.enabled;
        }

        if(Input.GetKeyDown(KeyCode.Mouse0))
        {
            m_Anim.Play("Attack");
        }
    }
}
