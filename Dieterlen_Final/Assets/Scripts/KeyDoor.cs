﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class KeyDoor : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag ("Player") && GameControlScript.keyCount >= 2)
        {
            GameControlScript.keyCount--;
            gameObject.SetActive (false);
            SceneManager.LoadScene("Level 2");
        }
    }
}
