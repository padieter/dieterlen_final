﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameControlScript : MonoBehaviour
{
    public GameObject gameOver, restartButton, exitButton;
    public static int keyCount;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (TimeLeftScript.timeLeft <= 0)
        {
            Time.timeScale = 0;
            gameOver.gameObject.SetActive(true);
            restartButton.gameObject.SetActive(true);
            exitButton.gameObject.SetActive(true);
        }
    }

    public void restartScene()
    {
        gameOver.gameObject.SetActive(false);
        restartButton.gameObject.SetActive(false);
        Time.timeScale = 1;
        TimeLeftScript.timeLeft = 60f;
        SceneManager.LoadScene("TilemapStart");
        exitButton.gameObject.SetActive(false);
    }

    public void exitGame()
    {
        Application.Quit();
        Debug.Log("Game is Exiting");
    }
}
