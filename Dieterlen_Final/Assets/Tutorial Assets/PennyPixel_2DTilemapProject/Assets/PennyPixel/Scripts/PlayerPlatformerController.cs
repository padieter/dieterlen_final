﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPlatformerController : PhysicsObject {

    public float maxSpeed = 7;
    public float jumpTakeOffSpeed = 7;

    private SpriteRenderer spriteRenderer;
    private Animator animator;

    public GameObject swordPrefab;
    public Transform swordSpawn;
    private GameObject sword;

    public GameObject gameOver, restartButton;

    const float Move_Delta = 0.2f;

    // Use this for initialization
    void Awake () 
    {
        spriteRenderer = GetComponent<SpriteRenderer> (); 
        animator = GetComponent<Animator> ();
        sword = GameObject.Find("Sword");
    }


    protected override void ComputeVelocity()
    {
        Vector2 move = Vector2.zero;

        move.x = Input.GetAxis("Horizontal");

        if (Input.GetButtonDown("Jump") && grounded)
        {
            velocity.y = jumpTakeOffSpeed;
        }
        else if (Input.GetButtonUp("Jump"))
        {
            if (velocity.y > 0)
            {
                velocity.y = velocity.y * 0.5f;
            }
        }

        bool flipSpriteLeft = (spriteRenderer.flipX ? (move.x > 0.01f) : (move.x < -0.01f));
         if (flipSpriteLeft)
         {
             transform.localScale = new Vector3(-1, 1, 1);
             /*spriteRenderer.flipX = !spriteRenderer.flipX;
             sword.transform.position = new Vector2(-.18f, -0.1f);
             sword.transform.Rotate(Vector3.forward * -90);*/
         }

         bool flipSpriteRight = (spriteRenderer.flipX ? (move.x < -0.01f) : (move.x > 0.01f));
         if (flipSpriteRight)
         {
             transform.localScale = new Vector3(1, 1, 1);
         }

        /*if (Mathf.Abs(move.x) > Move_Delta)
        {
            spriteRenderer.flipX = move.x > Move_Delta ? false : true;
            transform.localScale = new Vector3(-1, 1, 1);
        }*/


        animator.SetBool("grounded", grounded);
        animator.SetFloat("velocityX", Mathf.Abs(velocity.x) / maxSpeed);

        targetVelocity = move * maxSpeed;
    }

    void OnTriggerEnter2D(Collider2D theCollider)
    {
        if (theCollider.CompareTag("Enemy"))
        {
            GameOver();
        }
    }

    void GameOver()
    {
        Destroy(gameObject);
        gameOver.gameObject.SetActive(true);
        restartButton.gameObject.SetActive(true);
        Time.timeScale = 0;
    }
}